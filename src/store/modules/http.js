import {
  getTreeData,
  getFieldNames,
  sendSQL
} from '@/api'

/**
 * 发送http请求
 */

const state = {}

const mutations = {}

const actions = {
  async getTreeData({ commit }, params) {
    const data = await getTreeData(params)
    return data
  },
  async getFieldNames({ commit }, params) {
    const data = await getFieldNames(params)
    return data
  },
  async sendSQL({ commit }, params) {
    const data = await sendSQL(params)
    return data
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}